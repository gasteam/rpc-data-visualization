import streamlit as st
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import numpy as np
import psycopg2
from influxdb import DataFrameClient
import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots
import plotly.io as pio
import time


# ---- STREAMLIT APP CONFIGS ------------
plt.style.use(st.secrets["MATPLOTLIB_STYLE"])
markers = list(
    marker
    for marker in mpl.lines.Line2D.markers
    if marker
    not in [
        "o",
        ",",
    ]
)
dsn = st.secrets["DSN"]
host = st.secrets["HOSTNAME"]
mpl.rcParams["lines.markeredgewidth"] = 1
st.set_page_config(
    layout="wide", page_title="RPC data visualization", initial_sidebar_state="expanded"
)
query_params = st.query_params
ohmic_voltage = 6000
physics_voltage = 9400
grafana_links = {
    'gif': 'https://epdt-rd-monitoring.web.cern.ch/d/RdfFNo1mz/rpc-gif?orgId=1&from={}&to={}',
    '256': 'https://epdt-rd-monitoring.web.cern.ch/d/ZiK9EkPiz/lab256?orgId=1&from{}&to={}'
}

custom_plotly_template = go.layout.Template(
    layout=go.Layout(
        font=dict(family="Arial, sans-serif"),
        title=dict(font=dict(size=20)),
        legend=dict(font=dict(size=16)),
        xaxis=dict(title=dict(font=dict(size=16)), zerolinecolor=None, showgrid=True, gridwidth=1, gridcolor='black',
                   showline=True, linecolor='black', mirror=True, griddash='longdash', minor_griddash='dash'),
        yaxis=dict(title=dict(font=dict(size=16)), tickson="boundaries", ticks="inside", tickwidth=1, showticklabels=True, ticklen=10, zerolinecolor=None, showgrid=True, gridwidth=1,
                   gridcolor='black', showline=True, linecolor='black', rangemode='tozero', mirror=True, griddash='dash', minor_griddash='dot'),
        plot_bgcolor='white',

    ),

)
pio.templates['custom'] = custom_plotly_template
pio.templates.default = "simple_white+custom"

# ------- DATABASE CONNECTION CONFIGS ---------
influxdb_client = DataFrameClient(
    host=st.secrets["INFLUXDB_HOST"],
    port=st.secrets["INFLUXDB_PORT"],
    username=st.secrets["INFLUXDB_USER"],
    password=st.secrets["INFLUXDB_PASSWORD"],
    database=st.secrets["INFLUXDB_DATABASE"],
    ssl=st.secrets["INFLUXDB_SSL"],
    verify_ssl=st.secrets["INFLUXDB_VERIFYSSL"],
)
conn = st.connection("postgresql", type="sql")


# ------- GLOBAL DATABASE QUERIES -------------
gas_mixture_query = """
    SELECT DISTINCT id, label, value
    FROM gas_mixtures
"""

# This query uses a user defined function interpolate() on the database
# and for each run id performs a linear interpolation of the x and y v
linear_interpolation_query = """
WITH bounds AS (
    SELECT
        h.id as id, r.id as rpc,
        h.date_updated, date_trunc('day', h.timestamp_end) as date,
        h.name, g.label as gas_mixture, h.place,
        c.content as comment, h.timestamp_start, h.timestamp_end,
        h2.monitored_voltage, h2.monitored_voltage * :p0 / h2.pressure * ((h2.temperature + 273.15) / :t0) as x,
        h2."current" as y,
        h2.current_error as yerr,
        LEAD(h2.monitored_voltage * :p0 / h2.pressure * ((h2.temperature + 273.15) / :t0)) OVER (PARTITION BY h.id ORDER BY h2.monitored_voltage) AS next_x,
        LEAD(h2."current") OVER (PARTITION BY h.id ORDER BY h2.monitored_voltage) AS next_y,
        LEAD(h2."current_error") OVER (PARTITION BY h.id ORDER BY h2.monitored_voltage) AS next_yerr
    FROM hvscans h, hvpoints h2, rpcs r, comments c, gas_mixtures g
    where h.id = h2.hvscan_id 
    and h.rpc_id = r.id
    and h.comment_id = c.id
    and g.id = h.gas_mixture_id
    and h.timestamp_end is not null
    and (h.source_on = :source_on or :source_on is null)
    and h.timestamp_start::date >= :start_date
    and h.timestamp_end::date <= :end_date
    -- and h.upstream_abs = :upstream_abs
    -- and (h.config->'analysis'->'run'->'gamma'->>'source_on')::bool = :source_on
    AND (:gas_mixtures = '{}' OR h.gas_mixture_id = ANY(:gas_mixtures))
    -- and h.upstream_abs = :upstream_abs
)
SELECT
    id,
    -- date,
    timestamp_start::timestamp,
    timestamp_end,
    place,
    rpc,
    gas_mixture,
    name,
    comment,
    -- x as closest_corrected_voltage,
    interpolate(x, y, next_x, next_y, :ohmic_voltage) AS interpolated_ohmic_current,
    interpolate(x, y, next_x, next_yerr, :ohmic_voltage) AS interpolated_ohmic_current_error,
    interpolate(x, y, next_x, next_y, :physics_voltage) AS interpolated_physics_current,
    interpolate(x, y, next_x, next_yerr, :physics_voltage) AS interpolated_physics_current_error
FROM bounds
WHERE
    (:ohmic_voltage >= x AND :ohmic_voltage <= next_x
    OR
    :physics_voltage >= x AND :physics_voltage <= next_x)
    AND (:rpcs = '{}' OR rpc = ANY(:rpcs))
ORDER BY id desc
"""

influxdb_query = """
    SELECT mean(charge) * 1000 as integrated_charge
    FROM (
        SELECT cumulative_sum(mean("IMon")) * 60 / ({area}) / 1000000 as charge 
        FROM "hv"
        WHERE module='hvrpc256new'
        AND channel_name='{channel_name}'
        AND status_code = 1
        AND time > '{start_time}' and time <= '{end_time}'
        GROUP BY time(60s)
        fill(none)
    )
    GROUP BY time(1d)
    FILL(none)
"""


def plot_hvscans_over_time(hvscans_id: int | list[int]):
    # Plot the timeseries data ofthe HVscan
    # Get the RPC number, the minimum and maximum timestamp date
    for selected_id in hvscans_id:
        rpcs = conn.query("""SELECT DISTINCT rpc_id FROM hvscans WHERE id = :selected_id""", params={
            "selected_id": selected_id}).rpc_id
        start_time = conn.query("""SELECT min(timestamp_start) as time FROM hvscans WHERE id = :selected_id""", params={
                                "selected_id": selected_id}).time.iloc[0].strftime("%Y-%m-%dT%H:%M:%SZ")
        end_time = conn.query("""SELECT max(timestamp_end) as time FROM hvscans WHERE id = :selected_id""", params={
            "selected_id": selected_id}).time.iloc[0].tz_localize('Europe/Rome').tz_convert(None).strftime("%Y-%m-%dT%H:%M:%SZ")

        rpcs_query_formatted = " AND ".join(
            [f"channel_name= 'RPC{rpc}'" for rpc in rpcs])
        query = f"""
            SELECT mean(VMon) as voltage, mean(IMon) as current
            FROM hv
            WHERE {rpcs_query_formatted}
            AND time > '{start_time}' and time <= '{end_time}'
            GROUP BY time(10s)
            fill(none)"""
        data = influxdb_client.query(query)
        if 'hv' in data and len(data['hv']):
            df = data['hv'].tz_localize(None)
            # (df.index - df.index.min()).total_seconds() / 60
            df['time'] = df.index
            fig = make_subplots(specs=[[{"secondary_y": True}]])
            fig.add_trace(go.Scatter(
                x=df['time'], y=df['current'], mode='lines', name='current'), secondary_y=False,)
            fig.add_trace(go.Scatter(
                x=df['time'], y=df['voltage'], mode='lines', name='voltage'), secondary_y=True)
            fig.update_yaxes(
                title_text="Current [uA]", secondary_y=False)
            fig.update_yaxes(
                title_text="Voltage [V]", secondary_y=True)
            fig.update_layout(title=f"RPC{rpcs[0]} HVscan #{
                selected_id}", xaxis_title="Time [min]", template=custom_plotly_template,
                width=600, height=400,
                xaxis=dict(showgrid=True, gridcolor='black',),
                yaxis=dict(showgrid=False, gridcolor='black',))
            st.plotly_chart(fig, key=str(time.time()))


def plot_hvscans_from_events(events: dict):
    # Get the id
    selected_points = events["selection"]["points"]
    if len(selected_points):
        selected_ids = [point["customdata"][1]
                        for point in selected_points]

        # Plot the data of the HVscan
        hvscan_points_df = conn.query("""SELECT h.id::text, h.name,h.timestamp_start::date as date, h.timestamp_start as timestamp_start, 
                                      h.timestamp_end as time_end, h2.monitored_voltage, h2."current", h2.current_error,
                                      h2.timestamp_start as point_start, h2.timestamp_end as point_end
                    FROM hvscans h, hvpoints h2, comments c
                    WHERE h.id = h2.hvscan_id
                    AND h.comment_id = c.id
                    AND h.id = ANY(:selected_ids)""", params={"selected_ids": selected_ids})
        hvscan_df = conn.query("""SELECT h.id, h.timestamp_start as start_time, h.timestamp_end AT TIME ZONE 'Europe/Rome' as end_time, h.name, c.content as comment
                    FROM hvscans h, comments c
                        WHERE h.id = ANY(:selected_ids)
                        AND h.comment_id = c.id""", params={"selected_ids": selected_ids})

        fig_hvscan = px.scatter(
            hvscan_points_df,
            x="monitored_voltage",
            y="current",
            error_y="current_error",
            title="HVscan",
            labels={"current": "Current [uA]"},
            color="date",
            symbol="id",
            hover_name="name",
        )
        fig_hvscan.update_layout(
            width=600, height=400,
            title="Selected HVscans", template=custom_plotly_template)
        st.plotly_chart(fig_hvscan, use_container_width=False)
        # Plot the timeseries data ofthe HVscan
        # Get the RPC number, the minimum and maximum timestamp date
        plot_hvscans_over_time(selected_ids)
        # for selected_id in selected_ids:
        #     rpcs = conn.query("""SELECT DISTINCT rpc_id FROM hvscans WHERE id = :selected_id""", params={
        #         "selected_id": selected_id}).rpc_id
        #     start_time = conn.query("""SELECT min(timestamp_start) as time FROM hvscans WHERE id = :selected_id""", params={
        #                             "selected_id": selected_id}).time.iloc[0].strftime("%Y-%m-%dT%H:%M:%SZ")
        #     end_time = conn.query("""SELECT max(timestamp_end) as time FROM hvscans WHERE id = :selected_id""", params={
        #         "selected_id": selected_id}).time.iloc[0].tz_localize('Europe/Rome').tz_convert(None).strftime("%Y-%m-%dT%H:%M:%SZ")

        #     rpcs_query_formatted = " AND ".join(
        #         [f"channel_name= 'RPC{rpc}'" for rpc in rpcs])
        #     query = f"""
        #         SELECT mean(VMon) as voltage, mean(IMon) as current
        #         FROM hv
        #         WHERE {rpcs_query_formatted}
        #         AND time > '{start_time}' and time <= '{end_time}'
        #         GROUP BY time(10s)
        #         fill(none)"""
        #     data = influxdb_client.query(query)
        #     if 'hv' in data and len(data['hv']):
        #         df = data['hv'].tz_localize(None)
        #         df['time'] = df.index # (df.index - df.index.min()).total_seconds() / 60
        #         fig = make_subplots(specs=[[{"secondary_y": True}]])
        #         fig.add_trace(go.Scatter(
        #             x=df['time'], y=df['current'], mode='lines', name='current'), secondary_y=False,)
        #         fig.add_trace(go.Scatter(
        #             x=df['time'], y=df['voltage'], mode='lines', name='voltage'), secondary_y=True)
        #         fig.update_yaxes(
        #             title_text="Current [uA]", secondary_y=False)
        #         fig.update_yaxes(
        #             title_text="Voltage [V]", secondary_y=True)
        #         fig.update_layout(title=f"RPC{rpcs[0]} HVscan #{
        #                             selected_id}", xaxis_title="Time [min]", template=custom_plotly_template)
        #         st.plotly_chart(fig, use_container_width=True, key=str(time.time()))


# ---------- UTILITY FUNCTIONS ---------------
@st.fragment
def plot_currents_vs_time(df: pd.DataFrame):
    fig = px.scatter(
        df,
        x="timestamp_end",
        y="interpolated_ohmic_current",
        # error_y="interpolated_ohmic_current_error",
        facet_row="rpc",
        custom_data=["name"],
        hover_name="name",
        hover_data=["id", "comment", "timestamp_end"],
        height=400,
        template="simple_white+custom",
        title=f"{ohmic_voltage}V"
    )
    fig.update_yaxes(range=[0, None])
    events = st.plotly_chart(
        fig, use_container_width=True, selection_mode="points", on_select="rerun")
    if events:
        plot_hvscans_from_events(events)
    fig = px.scatter(
        df,
        x="timestamp_end",
        y="interpolated_physics_current",
        # error_y="interpolated_physics_current_error",
        facet_row="rpc",
        custom_data=["name"],
        hover_name="name",
        hover_data=["id", "comment", "timestamp_end"],
        height=400,
        template="simple_white+custom",
        title=f"{physics_voltage}V"
    )
    fig.update_yaxes(range=[0, None])
    events = st.plotly_chart(
        fig, use_container_width=True, selection_mode="points", on_select="rerun")
    if events:
        plot_hvscans_from_events(events)


@st.fragment
def plot_currents_vs_charge(df: pd.DataFrame):
    with st.spinner(text="Loading integrated charge data"):
        for rpc in rpcs:
            cursor = conn.engine.raw_connection().cursor()
            rpc_area_query = cursor.execute(
                "SELECT width, length FROM rpcs WHERE id = %s", (rpc,)
            )
            width, length = cursor.fetchone()
            cur.close()
            area = width * length
            query = influxdb_query.format(
                area=area,
                channel_name=f"RPC{rpc}",
                start_time=start_date,
                end_time=end_date,
            )
            integrated_charge = (
                influxdb_client.query(query)["hv"].tz_localize(
                    None).reset_index()
            )
            subset_df = df.query("rpc == @rpc").copy()
            merged = pd.merge_asof(
                subset_df.sort_values(by='timestamp_end'), integrated_charge, left_on="timestamp_end", right_on="index"
            )
            print(subset_df.head(), integrated_charge.head(), merged.head())
            col1, col2 = st.columns(2)
            with col1:
                fig_integrated = px.scatter(
                    merged,
                    x="integrated_charge",
                    y="interpolated_ohmic_current",
                    # error_y="interpolated_ohmic_current_error",
                    hover_name="name",
                    hover_data=[
                        "id",
                        "comment",
                        "timestamp_end",
                    ],
                    labels={"integrated_charge": "Integrated Charge [mC/cm2]"},
                    title=f"RPC{rpc} @ {ohmic_voltage}",
                    template="plotly_white+custom",
                )
                events = st.plotly_chart(fig_integrated,
                                         use_container_width=True, on_select="rerun", selection_mode="points")

            with col2:
                fig_integrated = px.scatter(
                    merged,
                    x="integrated_charge",
                    y="interpolated_physics_current",
                    # error_y="interpolated_physics_current_error",
                    hover_name="name",
                    hover_data=[
                        "id",
                        "comment",
                        "timestamp_end",
                    ],
                    labels={"integrated_charge": "Integrated Charge [mC/cm2]"},
                    title=f"RPC{rpc} @ {physics_voltage}",
                    template="plotly_white+custom",
                )
                events = st.plotly_chart(fig_integrated,
                                         use_container_width=True, on_select="rerun", selection_mode="points")


def plot_selected_hvscans(selected_ids: list[int]):
    # Retrieve the HVscans point from id of the HVscans
    hvscan_df = conn.query("""SELECT h.id::text, h.rpc_id as rpc, h.name, h.timestamp_start, ROUND(h2.monitored_voltage::numeric, 0) as voltage, 
                           ROUND(h2."current"::numeric, 2) as current, 
                           ROUND(h2.current_error::numeric, 2) as current_error, c.content as comment
                        FROM hvscans h, hvpoints h2, comments c
                        WHERE h.id = h2.hvscan_id
                        AND h.comment_id = c.id
                        AND h.id = ANY(:selected_ids)""", params={"selected_ids": selected_ids})
    fig = px.scatter(
        hvscan_df,
        x="voltage",
        y="current",
        error_y="current_error",
        color="id",
        symbol="rpc",
        hover_name="name",
        hover_data=["id", "rpc"],
        height=400,
        template="simple_white+custom",
    )
    fig.update_layout(width=600, height=400, xaxis=dict(showgrid=True, minor_griddash="dot", gridcolor='black',),
                      yaxis=dict(showgrid=True, gridcolor='black', minor_griddash="dot"), template=custom_plotly_template, plot_bgcolor='white')
    st.plotly_chart(fig, theme=None)


# --------- APP LAYOUT AND LOGIC ---------------
st.title("RPC data visualization")

st.info("Select one or more scans from the table to plot their HV scans", icon="ℹ️")

with st.sidebar:
    st.title("Filters")

    cur = conn.engine.raw_connection().cursor()
    cur.execute("SELECT DISTINCT(rpc) from runs ORDER by rpc")
    rpcs_choices = [r[0] for r in cur.fetchall()]
    rpcs = st.multiselect(
        label="RPC", options=rpcs_choices, default=None)

    start_date = query_params.get("start-period", "2023-03-01")
    start_date = pd.to_datetime(start_date)
    end_date = query_params.get("end-period", pd.to_datetime("now"))
    start_period = st.date_input(label="Start period", value=start_date)
    end_period = st.date_input(label="End period", value=end_date)

    cur.execute(gas_mixture_query)
    # gas_mixtures_choices = [r[0] for r in cur.fetchall()]
    gas_mixture_choices: dict = {r[0]: r[1] for r in cur.fetchall()}
    gas_mixture_labels = list(gas_mixture_choices.values())
    selected_gas_mixture_labels = st.multiselect(
        label="Gas mixture",
        options=gas_mixture_labels,
        default='STD'
    )
    selected_gas_mixture_ids = [
        key
        for key, value in gas_mixture_choices.items()
        if value in selected_gas_mixture_labels
    ]

    ohmic_voltage = st.number_input(
        label="Ohmic voltage for current interpolation [V]", value=ohmic_voltage)
    physics_voltage = st.number_input(
        label="Physics voltage for current interpolation [V]", value=physics_voltage)

    p0 = st.number_input(label="p0 [mbar]", value=965)
    t0 = st.number_input(label="t0 [K]", value=293.15)

    source_on = st.selectbox(label="Source on", options=[False, True, None])

    cur.execute(
        "SELECT DISTINCT( ROUND((r.config->'run'->'gamma'->'upstream_abs')::numeric, 1)) FROM runs r"
    )
    abs_choices = [r[0] for r in cur.fetchall()]
    upstream_abs = st.multiselect(
        label="Upstream ABS",
        options=abs_choices,
        disabled=False if source_on else True,
        default=[],
    )
    plot_integrated_charge = st.checkbox("Plot integrated charge", value=False)
    if plot_integrated_charge:
        title_button = "Plot filtered data vs. time and integrated charge"
    else:
        title_button = "Plot filtered data vs. time"
    plot_all_hvscans_button = st.button(label=title_button)


params = {
    "ohmic_voltage": ohmic_voltage,
    "physics_voltage": physics_voltage,
    "rpcs": rpcs,
    "p0": p0,
    "t0": t0,
    "source_on": source_on,
    "upstream_abs": upstream_abs or None,
    "gas_mixtures": selected_gas_mixture_ids,
    "start_date": start_period,
    "end_date": end_period,
}


@st.fragment
def write_dataframe(params):
    df = conn.query(linear_interpolation_query, params=params, ttl=60)
    selected_rows = st.dataframe(df.drop(columns=['timestamp_start']), selection_mode="multi-row", hide_index=True, on_select="rerun", column_config={
        "timestamp_end": st.column_config.DatetimeColumn("Timestamp", format="YYYY-MM-DD HH:mm"),
        "interpolated_ohmic_current": st.column_config.ProgressColumn(f"Current @ {ohmic_voltage} V", min_value=0, max_value=10, format="%.1f uA"),
        "interpolated_physics_current": st.column_config.ProgressColumn(f"Current @ {physics_voltage} V", min_value=0, max_value=df.interpolated_physics_current.max(), format="%.1f uA"),
    })

    with st.expander("Plots of selected HVscans", expanded=True):
        if len(selected_rows['selection']['rows']):
            if len(selected_rows['selection']['rows']):
                selected_ids = df.iloc[selected_rows['selection']
                                       ['rows']].id.tolist()

                # Create a column in the dataframe with the proper grafana link
                # Need to format the timestamp_start and timestamp_end into epoch time in ms
                df['grafana_link'] = df.apply(lambda x: grafana_links[x['place']].format(int(x.timestamp_start.tz_localize(
                    'Europe/Rome').tz_convert(None).timestamp() * 1000), int(x.timestamp_end.timestamp() * 1000)), axis=1)
                print(df[['timestamp_start', 'timestamp_end', 'grafana_link']])
                st.dataframe(df.loc[selected_rows['selection']['rows'], [
                    'id', 'rpc', 'comment', 'grafana_link']], column_config={
                    "grafana_link": st.column_config.LinkColumn("Grafana link", display_text="Grafana link")
                }, hide_index=True)
                plot_selected_hvscans(selected_ids)
                plot_hvscans_over_time(selected_ids)
    with st.expander("Plots of the runs in the table", expanded=True):
        if plot_all_hvscans_button:
            st.info('Click on a point of the plot below to see its corresponding hvscan. Hold Shift + click to select multiple points ', icon="ℹ️")
            plot_currents_vs_time(df)
            if plot_integrated_charge:
                plot_currents_vs_charge(df)

        return df


df = write_dataframe(params=params)
