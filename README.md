# RPC Data Visualization

Data visualization project using streamlit for RPC data from EP-DT gas team.

# Installation

To run this web app locally, you can either install the required pip packages on your local python installation.
If using venv with python 3:

```shell
git clone https://gitlab.cern.ch/gasteam/rpc-data-visualization.git
cd rpc-data-visualization
python -m venv venv
source venv/bin/activate
python -m pip install -r requirements.txt
streamlit run visualization.py
```

The app can be run also using docker through the `Dockerfile` image provided:
```shell
docker build -t rpc-data-visualization .
docker run -p 8080:8080
-e DSN=<DB connection string>
 rpc-data-visualization
```