import streamlit as st
import pandas as pd
import psycopg2
from psycopg2 import sql
import matplotlib.pyplot as plt
import seaborn as sns
from st_aggrid import AgGrid, GridOptionsBuilder, ColumnsAutoSizeMode, GridUpdateMode
import matplotlib as mpl
import numpy as np
from streamlit_option_menu import option_menu
import olefin
import urllib
from pathlib import Path


def fit_efficiency_plot(*args, **kwargs):
    df = kwargs["data"]
    row = df.iloc[0]
    xfit = np.arange(df.voltage.min(), df.voltage.max(), 20)
    yfit = olefin.EPDTRunAnalysis.efficiency_model(
        xfit, row.effmax, row.gamma, row.hv50
    )
    plt.plot(xfit, yfit * 100, "-", color=kwargs["color"])


plt.style.use(st.secrets["MATPLOTLIB_STYLE"])
markers = list(
    marker
    for marker in mpl.lines.Line2D.markers
    if marker
    not in [
        "o",
        ",",
    ]
)
dsn = st.secrets["DSN"]
host = st.secrets["HOSTNAME"]
mpl.rcParams["lines.markeredgewidth"] = 1


st.set_page_config(
    layout="wide",
    page_title="RPC data visualization",
    initial_sidebar_state="collapsed",
)

query_params = st.query_params


@st.cache_resource
def init_connection():
    return psycopg2.connect(dsn)


conn = init_connection()

gas_mixture_query = """
    SELECT t.gas_mixture 
    FROM (
        SELECT distinct(r.gas_mixture), max(r.date) as last_date
        FROM runs r
        WHERE r.date >= %(start_date)s
        AND r.date <= %(end_date)s
        GROUP BY r.gas_mixture
        ORDER BY last_date DESC
    ) t
"""
single_gas_query = ""

tags_query = """
    select jsonb_array_elements(subquery.tags) from 
    (select distinct(h.config->'run'->'tags') as tags
    from hvscans h
    where (h.config->'run'->>'tags')::jsonb is not null) as subquery
"""

cur = conn.cursor()

with st.sidebar:

    cur.execute(tags_query)
    tags_choices = [r[0] for r in cur.fetchall()]
    tags = st.multiselect(label="Run tags", options=tags_choices)

    cur.execute("SELECT DISTINCT(rpc) from runs ORDER by rpc")
    rpcs_choices = [r[0] for r in cur.fetchall()]
    rpcs = st.multiselect(label="RPC", options=rpcs_choices)

    cur.execute("SELECT DISTINCT(r.config->'run'->>'place') FROM runs r")
    places_choices = [r[0] for r in cur.fetchall()]
    places = st.selectbox(label="Place", options=places_choices)

    cur.execute("SELECT min(r.date) as start_date, max(r.date) as end_date FROM runs r")
    start_date, end_date = [r for r in cur.fetchall()][0]
    start_date = st.date_input(label="Start date", value=start_date)
    end_date = st.date_input(label="End date", value=end_date)

    cur.execute(gas_mixture_query, dict(start_date=start_date, end_date=end_date))
    gas_mixtures_choices = [r[0] for r in cur.fetchall()]
    gas_mixtures = st.multiselect(label="Gas mixture", options=gas_mixtures_choices)

    source_on = st.selectbox(label="Source on", options=[None, True, False])

    cur.execute(
        "SELECT DISTINCT( ROUND((r.config->'run'->'gamma'->'upstream_abs')::numeric, 1)) FROM runs r"
    )
    abs_choices = [r[0] for r in cur.fetchall()]
    upstream_abs = st.multiselect(
        label="Upstream ABS",
        options=abs_choices,
        disabled=False if source_on else True,
        default=[],
    )

    discard_bad_runs = st.checkbox(label="Discard quick or bad runs?", value=True)

cur.close()

discard_bad_runs_query = ""
if discard_bad_runs:
    discard_bad_runs_query = """
        AND r.config->>'notes' NOT ILIKE ALL(ARRAY['%%bad%%', '%%fast%%', '%%wrong%%', '%%quick%%', '%%discard%%', '%%test%%', '%%check%%', '%%try%%'])
        AND r.folder_path NOT ILIKE ALL(ARRAY['%%bad%%', '%%fast%%', '%%quick%%', '%%wrong%%', '%%discard%%', '%%test%%', '%%check%%', '%%try%%'])
    """

if len(tags):
    tags_query = f"""
        EXISTS (
            SELECT * 
            FROM jsonb_array_elements_text((r.config->'run'->'tags')::jsonb) x
            WHERE x = ALL(%(tags)s)
        )
    """
else:
    tags_query = "1=1"

if len(query_params_ids := query_params.get("ids", [])):
    formatted_ids = [int(item) for item in query_params_ids]
    sql_query = sql.SQL(
        """
        SELECT id, rpc, date, r.config->'run'->>'place' place, r.name, r.gas_mixture, (r.config->'run'->'gamma'->>'source_on')::bool source_on,
        ROUND((r.config->'run'->'gamma'->'upstream_abs')::numeric, 1) upstream_abs, ROUND((r.config ->'run'->'t')::numeric, 2) as "temperature (C)", 
        ROUND((r.config->'run'->'p')::numeric, 2) AS "pressure (mbar)", 
        r.config->'run'->>'tags' as tags,
        r.config->'notes' as notes, folder_path
        FROM runs r
        WHERE r.id = ANY(%(formatted_ids)s)
    """
    )
else:
    formatted_ids = []
    sql_query = sql.SQL(
        """
        SELECT id, rpc, date, r.config->'run'->>'place' place, r.name, r.gas_mixture, (r.config->'run'->'gamma'->>'source_on')::bool source_on,
        ROUND((r.config->'run'->'gamma'->'upstream_abs')::numeric, 1) upstream_abs, ROUND((r.config ->'run'->'t')::numeric, 2) as "temperature (C)", 
        ROUND((r.config->'run'->'p')::numeric, 2) as "pressure (mbar)",
        r.config->'run'->>'tags' as tags,
        r.config->'notes' as notes,  folder_path
        FROM runs r
        WHERE """
        + tags_query
        + """
        AND ((r.rpc = ANY(%(rpcs)s)) OR (%(rpcs)s IS NULL))
        AND (ROUND((r.config->'run'->'gamma'->'upstream_abs')::numeric, 1) = ANY(%(upstream_abs)s)  OR %(upstream_abs)s IS NULL  )
        AND ((r.config->'run'->>'place' = %(places)s) OR (%(places)s IS NULL))
        AND (((r.config->'run'->'gamma'->>'source_on')::bool = %(source_on)s) OR (%(source_on)s IS NULL))
        AND ((r.gas_mixture = ANY(%(gas_mixture)s)) OR (%(gas_mixture)s IS NULL))
        AND r.date >= %(start_date)s AND r.date <= %(end_date)s
        """
        + single_gas_query
        + discard_bad_runs_query
        + """
        ORDER BY r.date DESC
        LIMIT 200
    """
    )

df = pd.read_sql_query(
    sql_query.as_string(conn),
    conn,
    params={
        "tags": tags,
        "rpcs": rpcs or None,
        "upstream_abs": upstream_abs or None,
        "places": places,
        "source_on": source_on,
        "gas_mixture": gas_mixtures or None,
        "formatted_ids": formatted_ids or None,
        "start_date": start_date,
        "end_date": end_date,
    },
)

if len(formatted_ids):
    pre_selected_rows = list(range(df.shape[0]))
else:
    pre_selected_rows = None

grid_options_builder = GridOptionsBuilder.from_dataframe(df)
grid_options_builder.configure_selection(
    selection_mode="multiple",
    use_checkbox=True,
    header_checkbox=True,
    pre_selected_rows=pre_selected_rows,
)
grid_options_builder.configure_side_bar()
grid_options_builder.configure_pagination(
    enabled=True, paginationAutoPageSize=True, paginationPageSize=20
)
grid_options_builder.configure_grid_options()


st.text(f"{df.shape[0]} rows fetched")


with st.form("id selection", clear_on_submit=False):

    table_options = grid_options_builder.build()
    table = AgGrid(
        df,
        table_options,
        height=500,
        columns_auto_size_mode=ColumnsAutoSizeMode.FIT_CONTENTS,
        update_mode=GridUpdateMode.SELECTION_CHANGED,
    )

    selected_plot = option_menu(
        None,
        ["📈 HV plots", "📉 Rate plots", "📊 Distribution plots", "🌊 Raw Waveforms"],
        orientation="horizontal",
    )
    if selected_plot == "📈 HV plots":
        plot_selection = st.multiselect(
            label="Plots to make",
            options=[
                "eff",
                "charge",
                "currents",
                "clusize",
                "timeres",
                "tot",
                "stprob",
                "effstprob",
            ],
            default=["eff", "charge"],
        )
    elif selected_plot == "📉 Rate plots":
        plot_selection = st.multiselect(
            label="Plots to make",
            options=["currents", "wp", "clusize", "timeres", "effmax", "abs", "charge"],
            default=["currents", "wp", "effmax"],
        )

    if selected_plot == "🌊 Raw Waveforms":
        num_waveforms = st.number_input(
            label="Number of random waveforms to retrieve", value=1
        )
    if selected_plot != "🌊 Raw Waveforms":
        group_rpcs = True if "group_rpcs" in query_params else False
        checkbox_group_rpcs = st.checkbox(
            label="Group plots by RPCs", key="group_rpcs", value=group_rpcs
        )
    submitted = st.form_submit_button("Plot runs")
    newline = "\n"

    ids = []
    if table["selected_rows"] is not None:
        ids = [int(row["id"]) for _, row in table["selected_rows"].iterrows()]

    if submitted:
        if not len(ids):
            st.warning("No rows selected!")
            st.stop()

        link = urllib.parse.urlencode(dict(ids=ids), doseq=True)
        if selected_plot != "🌊 Raw Waveforms":
            st.markdown(
                f"Share link: {host}?{link}{'&group_rpcs=True' if checkbox_group_rpcs else ''}"
            )

        if selected_plot == "📈 HV plots":
            with st.spinner("Making plots"):
                dfacq = pd.read_sql(
                    f"""
                    SELECT r.id, r.rpc, r.date, r.gas_mixture, r.name, r.effmax, r.gamma, r.hv50, r.working_point,
                    a.voltage, a.voltage - r.working_point hvrel, a.efficiency * 100 efficiency, a.efficiency_error * 100 efficiency_error, 
                    a.streamer_probability * 100 streamer_probability, a.streamer_probability_error * 100 streamer_probability_error,
                    a.avalanche, a.streamer, a.currents_standard_mean, a.time_over_threshold, a.time_resolution, a.cluster_size,
                    {   f"r.id || ' - ' || r.date || ' - Name: ' || r.name || '{newline}' || r.gas_mixture " if checkbox_group_rpcs
                        else f"r.id || ' - ' || r.date || ' - RPC' || r.rpc || ' - Name: ' || r.name || '{newline}' || r.gas_mixture"
                    } AS label
                    
                    FROM runs r, acquisitions a
                    WHERE 
                    r.id = a.run_id
                    AND r.id = ANY(%s)
                    ORDER BY COALESCE((r.config->'run'->'gas_mixture'->'R134A')::numeric, 0) DESC,
                            a.voltage ASC
                """,
                    conn,
                    params=(ids,),
                )
                if not len(dfacq):
                    st.warning("No rows found in database!")
                    st.stop()

                if checkbox_group_rpcs:
                    if "eff" in plot_selection:
                        g = sns.FacetGrid(
                            data=dfacq,
                            col="rpc",
                            hue="label",
                            col_wrap=3,
                            height=4,
                            aspect=1.2,
                            hue_kws={"marker": markers},
                            sharex=False,
                            sharey=False,
                            ylim=(0, 100),
                            despine=False,
                        )
                        g.map_dataframe(fit_efficiency_plot)
                        g.map_dataframe(
                            plt.errorbar,
                            "voltage",
                            "efficiency",
                            "efficiency_error",
                            linestyle="",
                        )
                        # g.map(plt.errorbar, 'voltage', 'streamer_probability', 'streamer_probability_error', linestyle='--')
                        g.add_legend(
                            title="Label", loc="lower center", bbox_to_anchor=(0, 1.05)
                        )
                        g.set_axis_labels(
                            "HV$_{eff}$ [V]", "Efficiency -, St. Prob. -- [%]"
                        )
                        for ax in g.axes.flat:
                            ax.yaxis.set_major_locator(mpl.ticker.LinearLocator(11))
                        g.tight_layout()
                        st.pyplot(g.fig)

                    if "charge" in plot_selection:
                        g = sns.FacetGrid(
                            data=dfacq,
                            col="rpc",
                            hue="label",
                            col_wrap=3,
                            height=4,
                            aspect=1.2,
                            hue_kws={
                                "marker": markers,
                                "edgecolor": sns.color_palette(),
                            },
                            sharex=False,
                            sharey=False,
                            despine=False,
                        )
                        g.map(sns.scatterplot, "hvrel", "avalanche")
                        g.map(sns.scatterplot, "hvrel", "streamer", facecolors="none")
                        g.add_legend(title="Label")
                        g.set(title="Mean prompt charge", yscale="log")
                        g.set_axis_labels("HV$_{eff}$ [V]", "Charge [pC]")
                        g.tight_layout()
                        st.pyplot(g.fig)

                    if "currents" in plot_selection:
                        g = sns.FacetGrid(
                            data=dfacq,
                            col="rpc",
                            hue="label",
                            col_wrap=3,
                            height=4,
                            aspect=1.2,
                            hue_kws={
                                "marker": markers,
                                "edgecolor": sns.color_palette(),
                            },
                            sharex=False,
                            sharey=False,
                            despine=False,
                        )
                        g.map(sns.scatterplot, "hvrel", "currents_standard_mean")
                        g.add_legend(title="Label")
                        g.set(title="Currents")
                        g.set_axis_labels("HV$_{eff}$ [V]", "Current [uA]")
                        g.tight_layout()
                        st.pyplot(g.fig)

                    if "clusize" in plot_selection:
                        g = sns.FacetGrid(
                            data=dfacq,
                            col="rpc",
                            hue="label",
                            col_wrap=3,
                            height=4,
                            aspect=1.2,
                            hue_kws={
                                "marker": markers,
                                "edgecolor": sns.color_palette(),
                            },
                            sharex=False,
                            sharey=False,
                            despine=False,
                        )
                        g.map(sns.scatterplot, "hvrel", "cluster_size")
                        g.add_legend(title="Label")
                        g.set(title="Cluster size")
                        g.set_axis_labels("HV$_{eff}$ [V]", "Cluster size [#strips]")
                        g.tight_layout()
                        st.pyplot(g.fig)

                    if "timeres" in plot_selection:
                        g = sns.FacetGrid(
                            data=dfacq,
                            col="rpc",
                            hue="label",
                            col_wrap=3,
                            height=4,
                            aspect=1.2,
                            hue_kws={
                                "marker": markers,
                                "edgecolor": sns.color_palette(),
                            },
                            sharex=False,
                            sharey=False,
                            despine=False,
                        )
                        g.map(sns.scatterplot, "hvrel", "time_resolution")
                        g.add_legend(title="Label")
                        g.set(title="Time resolution")
                        g.set_axis_labels("HV$_{eff}$ [V]", "Time resolution [ns]")
                        g.tight_layout()
                        st.pyplot(g.fig)

                    if "tot" in plot_selection:
                        g = sns.FacetGrid(
                            data=dfacq,
                            col="rpc",
                            hue="label",
                            col_wrap=3,
                            height=4,
                            aspect=1.2,
                            hue_kws={
                                "marker": markers,
                                "edgecolor": sns.color_palette(),
                            },
                            sharex=False,
                            sharey=False,
                            despine=False,
                        )
                        g.map(sns.scatterplot, "hvrel", "time_over_threshold")
                        g.add_legend(title="Label")
                        g.set(title="Time resolution")
                        g.set_axis_labels("HV$_{eff}$ [V]", "Time over threshold [ns]")
                        g.tight_layout()
                        st.pyplot(g.fig)

                    if "stprob" in plot_selection:
                        g = sns.FacetGrid(
                            data=dfacq,
                            col="rpc",
                            hue="label",
                            col_wrap=3,
                            height=4,
                            aspect=1.2,
                            hue_kws={
                                "marker": markers,
                                "edgecolor": sns.color_palette(),
                            },
                            sharex=False,
                            sharey=False,
                            despine=False,
                        )
                        g.map(sns.scatterplot, "hvrel", "streamer_probability")
                        g.add_legend(title="Label")
                        g.set(title="Streamer probability", ylim=(0, 50))
                        g.set_axis_labels("HV$_{eff}$ [V]", "Streamer probability [%]")
                        g.tight_layout()
                        st.pyplot(g.fig)
                else:
                    fig, axs = plt.subplot_mosaic(
                        [
                            [
                                "eff" if "eff" in plot_selection else ".",
                                "charge" if "charge" in plot_selection else ".",
                                "currents" if "currents" in plot_selection else ".",
                            ],
                            [
                                "clusize" if "clusize" in plot_selection else ".",
                                "timeres" if "timeres" in plot_selection else ".",
                                "tot" if "tot" in plot_selection else ".",
                            ],
                            [
                                "stprob" if "stprob" in plot_selection else ".",
                                "effstprob" if "effstprob" in plot_selection else ".",
                                ".",
                            ],
                        ],
                        figsize=(4 * 3, 3 * 4),
                        dpi=300,
                    )

                    for ix, (label, group) in enumerate(
                        dfacq.groupby("label", sort=False)
                    ):
                        row = group.iloc[0, :]
                        xfit = np.linspace(
                            group.voltage.min(), group.voltage.max() + 200, 100
                        )
                        yfit = (
                            olefin.EPDTRunAnalysis.efficiency_model(
                                xfit, row.effmax, row.gamma, row.hv50
                            )
                            * 100
                        )
                        if "eff" in axs:
                            axs["eff"].errorbar(
                                group.voltage,
                                group.efficiency,
                                group.efficiency_error,
                                marker=markers[ix],
                                color=f"C{ix}",
                                label=label,
                                linestyle="",
                            )
                            axs["eff"].plot(xfit, yfit, "-", color=f"C{ix}")
                        if "effstprob" in axs:
                            axs["effstprob"].errorbar(
                                group.voltage,
                                group.efficiency,
                                group.efficiency_error,
                                marker=markers[ix],
                                color=f"C{ix}",
                                label=label,
                                linestyle="",
                            )
                            axs["effstprob"].errorbar(
                                group.voltage,
                                group.streamer_probability,
                                group.streamer_probability_error,
                                marker=markers[ix],
                                color=f"C{ix}",
                                linestyle="",
                            )
                            axs["effstprob"].plot(xfit, yfit, "-", color=f"C{ix}")
                            axs["effstprob"].axvline(
                                row.working_point, color=f"C{ix}", linestyle="--"
                            )
                    # sns.scatterplot(data=dfacq, x='voltage', y='efficiency', style='label', ax=axs['eff'], hue='label', palette='tab10')
                    if "eff" in axs:
                        axs["eff"].legend(loc="lower left", bbox_to_anchor=(0, 1.05))
                        axs["eff"].set(
                            xlabel="HV$_{eff}$", ylabel="Efficiency [%]", ylim=(0, 100)
                        )
                    if "effstprob" in axs:
                        axs["effstprob"].set(
                            xlabel="HV$_{eff}$", ylabel="Efficiency [%]", ylim=(0, 100)
                        )

                    if "charge" in axs:
                        sns.scatterplot(
                            data=dfacq,
                            x="hvrel",
                            y="avalanche",
                            style="label",
                            ax=axs["charge"],
                            legend=None,
                            hue="label",
                            palette="tab10",
                        )
                        sns.scatterplot(
                            data=dfacq,
                            x="hvrel",
                            y="streamer",
                            style="label",
                            ax=axs["charge"],
                            hue="label",
                            legend=None,
                            palette="tab10",
                        )
                        axs["charge"].set(
                            xlabel="HV$_{eff}$",
                            ylabel="Charge [pC]",
                            title="Mean prompt charge",
                            yscale="log",
                            ylim=(0, None),
                        )

                    if "currents" in axs:
                        sns.scatterplot(
                            data=dfacq,
                            x="hvrel",
                            y="currents_standard_mean",
                            style="label",
                            ax=axs["currents"],
                            hue="label",
                            legend=None,
                            palette="tab10",
                        )
                        min_currents = 0
                        max_currents = dfacq.currents_standard_mean.max()
                        ylim = (
                            min_currents - max(0.1 * min_currents, 0),
                            max_currents + max_currents * 0.1,
                        )
                        axs["currents"].set(
                            xlabel="HV$_{eff}$",
                            ylabel="Current [uA]",
                            title="Currents",
                            ylim=ylim,
                        )

                    if "clusize" in axs:
                        sns.scatterplot(
                            data=dfacq,
                            x="hvrel",
                            y="cluster_size",
                            style="label",
                            ax=axs["clusize"],
                            hue="label",
                            legend=None,
                            palette="tab10",
                        )
                        min_clusize = dfacq.cluster_size.min()
                        max_clusize = dfacq.cluster_size.max()
                        ylim = (
                            min_clusize - max(0.1 * min_clusize, 0),
                            max_clusize + max_clusize * 0.1,
                        )
                        axs["clusize"].set(
                            xlabel="HV$_{eff}$",
                            ylabel="Cluster size [# strips]",
                            title="Cluster size",
                            ylim=ylim,
                        )

                    if "timeres" in axs:
                        sns.scatterplot(
                            data=dfacq,
                            x="hvrel",
                            y="time_resolution",
                            style="label",
                            ax=axs["timeres"],
                            hue="label",
                            legend=None,
                            palette="tab10",
                        )
                        min_timeres = 0
                        max_timeres = 4
                        ylim = (
                            min_timeres - max(0.1 * min_timeres, 0),
                            max_timeres + max_timeres * 0.1,
                        )
                        axs["timeres"].set(
                            xlabel="HV$_{eff}$",
                            ylabel="Time resolution [ns]",
                            title="Raw time resolution",
                            ylim=ylim,
                        )

                    if "tot" in axs:
                        sns.scatterplot(
                            data=dfacq,
                            x="hvrel",
                            y="time_over_threshold",
                            style="label",
                            ax=axs["tot"],
                            hue="label",
                            legend=None,
                            palette="tab10",
                        )
                        axs["tot"].set(
                            xlabel="HV$_{eff}$",
                            ylabel="Time over threshold [ns]",
                            title="Time over threshold",
                            ylim=(0, 80),
                        )

                    if "stprob" in axs:
                        sns.scatterplot(
                            data=dfacq,
                            x="hvrel",
                            y="streamer_probability",
                            style="label",
                            ax=axs["stprob"],
                            hue="label",
                            legend=None,
                            palette="tab10",
                        )
                        axs["stprob"].set(
                            xlabel="HV$_{eff}$",
                            ylabel="Streamer probability [%]",
                            title="Streamer probability",
                            ylim=(0, 50),
                        )

                    fig.subplots_adjust(wspace=0.3, hspace=0.3)
                    st.pyplot(fig)

        elif selected_plot == "📉 Rate plots":
            with st.spinner("Making plots"):
                dfruns = pd.read_sql(
                    f"""
                    SELECT r.rpc, r.date, r.rate, r.gas_mixture, r.name, r.effmax * 100 effmax, r.gamma, r.hv50, r.working_point,
                    r.currents_standard_mean, r.time_over_threshold, r.time_resolution, r.cluster_size,
                    r.currents_standard_mean / NULLIF(r.rate, 0) * 100 as charge_per_count,
                    {   f"r.gas_mixture " if checkbox_group_rpcs
                        else f"'RPC' || r.rpc || '{newline}' || r.gas_mixture"
                    } AS label,
                    CASE 
                        WHEN (r.config->'run'->'gamma'->'source_on')::numeric = 0 THEN 'Source\nOff'
                        ELSE ROUND((r.config->'run'->'gamma'->'upstream_abs')::numeric, 1)::text
                    END AS upstream_abs
                    FROM runs r
                    WHERE r.id = ANY(%s)
                    ORDER BY COALESCE((r.config->'run'->'gas_mixture'->'R134A')::numeric, 0) DESC
                """,
                    conn,
                    params=(ids,),
                )

                if not len(dfruns):
                    st.warning("No rows")
                    st.stop()

                if checkbox_group_rpcs:
                    if "currents" in plot_selection:
                        g = sns.FacetGrid(
                            data=dfruns,
                            col="rpc",
                            hue="label",
                            col_wrap=3,
                            height=4,
                            aspect=1.2,
                            hue_kws={"marker": markers},
                            sharex=False,
                            sharey=False,
                            despine=False,
                        )
                        g.map(sns.scatterplot, "rate", "currents_standard_mean")
                        g.add_legend(title="Label")
                        g.set_axis_labels("Rate [Hz/cm$^2$]", "Currents [uA]")
                        g.fig.suptitle("Currents vs Rate")
                        g.tight_layout()
                        st.pyplot(g.fig)

                    if "wp" in plot_selection:
                        g = sns.FacetGrid(
                            data=dfruns,
                            col="rpc",
                            hue="label",
                            col_wrap=3,
                            height=4,
                            aspect=1.2,
                            hue_kws={"marker": markers},
                            sharex=False,
                            sharey=False,
                            despine=False,
                        )
                        g.map(sns.scatterplot, "rate", "working_point")
                        g.add_legend(title="Label")
                        g.set_axis_labels("Rate [Hz/cm$^2$]", "Working point [V]")
                        g.fig.suptitle("Working point vs Rate")
                        g.tight_layout()
                        st.pyplot(g.fig)

                    if "effmax" in plot_selection:
                        g = sns.FacetGrid(
                            data=dfruns,
                            col="rpc",
                            hue="label",
                            col_wrap=3,
                            height=4,
                            aspect=1.2,
                            hue_kws={"marker": markers},
                            sharex=False,
                            sharey=False,
                            despine=False,
                        )
                        g.map(sns.scatterplot, "rate", "effmax")
                        g.add_legend(title="Label")
                        g.set_axis_labels("Rate [Hz/cm$^2$]", "Efficiency [%]")
                        g.fig.suptitle("Maximum efficiency vs Rate")
                        g.tight_layout()
                        st.pyplot(g.fig)

                    if "clusize" in plot_selection:
                        g = sns.FacetGrid(
                            data=dfruns,
                            col="rpc",
                            hue="label",
                            col_wrap=3,
                            height=4,
                            aspect=1.2,
                            hue_kws={"marker": markers},
                            sharex=False,
                            sharey=False,
                            despine=False,
                        )
                        g.map(sns.scatterplot, "rate", "cluster_size")
                        g.add_legend(title="Label")
                        g.set_axis_labels("Rate [Hz/cm$^2$]", "Cluster size [#strips]")
                        g.fig.suptitle("Cluster size vs Rate")
                        g.tight_layout()
                        st.pyplot(g.fig)

                    if "timeres" in plot_selection:
                        g = sns.FacetGrid(
                            data=dfruns,
                            col="rpc",
                            hue="label",
                            col_wrap=3,
                            height=4,
                            aspect=1.2,
                            hue_kws={"marker": markers},
                            sharex=False,
                            sharey=False,
                            despine=False,
                        )
                        g.map(sns.scatterplot, "rate", "time_resolution")
                        g.add_legend(title="Label")
                        g.set_axis_labels("Rate [Hz/cm$^2$]", "Time resolution [ns]")
                        g.fig.suptitle("Time resolution vs Rate")
                        g.tight_layout()
                        st.pyplot(g.fig)

                    if "abs" in plot_selection:
                        g = sns.FacetGrid(
                            data=dfruns.sort_values(by="upstream_abs"),
                            col="rpc",
                            hue="label",
                            col_wrap=3,
                            height=4,
                            aspect=1.2,
                            hue_kws={"marker": markers},
                            sharex=False,
                            sharey=False,
                            despine=False,
                        )
                        g.map(sns.barplot, "upstream_abs", "currents_standard_mean")
                        g.add_legend(title="Label")
                        g.set_axis_labels("ABS filter", "Currents [uA]")
                        g.fig.suptitle("Currents vs ABS filter")
                        g.tight_layout()
                        st.pyplot(g.fig)

                    if "charge" in plot_selection:
                        g = sns.FacetGrid(
                            data=dfruns,
                            col="rpc",
                            hue="label",
                            col_wrap=3,
                            height=4,
                            aspect=1.2,
                            hue_kws={"marker": markers},
                            sharex=False,
                            sharey=False,
                            despine=False,
                        )
                        g.map(sns.scatterplot, "rate", "charge_per_count")
                        g.add_legend(title="Label")
                        g.set(title="Charge per count", ylim=(0, 100))
                        g.set_axis_labels("ABS filter", "Charge [pC]")
                        g.fig.suptitle("Charge per count vs Rate")
                        g.tight_layout()
                        st.pyplot(g.fig)

                else:
                    fig, axs = plt.subplot_mosaic(
                        [
                            [
                                "currents" if "currents" in plot_selection else ".",
                                "wp" if "wp" in plot_selection else ".",
                                "clusize" if "clusize" in plot_selection else ".",
                            ],
                            [
                                "timeres" if "timeres" in plot_selection else ".",
                                "effmax" if "effmax" in plot_selection else ".",
                                "abs" if "abs" in plot_selection else ".",
                            ],
                            ["charge" if "charge" in plot_selection else ".", ".", "."],
                        ],
                        figsize=(4 * 4, 3 * 4),
                        dpi=300,
                    )

                    if "currents" in plot_selection:
                        sns.scatterplot(
                            data=dfruns,
                            x="rate",
                            y="currents_standard_mean",
                            style="label",
                            ax=axs["currents"],
                            hue="label",
                            palette="tab10",
                        )
                        axs["currents"].legend(
                            loc="lower left", bbox_to_anchor=(0, 1.05)
                        )
                        axs["currents"].set(
                            xlabel="Rate [Hz/cm$^2$]",
                            ylabel="Currents [uA]",
                            xlim=(0, None),
                        )

                    if "wp" in plot_selection:
                        sns.scatterplot(
                            data=dfruns,
                            x="rate",
                            y="working_point",
                            style="label",
                            ax=axs["wp"],
                            hue="label",
                            palette="tab10",
                            legend=None,
                        )
                        axs["wp"].set(
                            xlabel="Rate [Hz/cm$^2$]",
                            ylabel="Voltage [V]",
                            title="Working point shift",
                            xlim=(0, None),
                        )

                    if "clusize" in plot_selection:
                        sns.scatterplot(
                            data=dfruns,
                            x="rate",
                            y="cluster_size",
                            style="label",
                            ax=axs["clusize"],
                            hue="label",
                            palette="tab10",
                            legend=None,
                        )
                        axs["clusize"].set(
                            xlabel="Rate [Hz/cm$^2$]",
                            ylabel="Cluster size [# strips]",
                            title="Cluster size",
                            xlim=(0, None),
                            ylim=(1, 3),
                        )

                    if "timeres" in plot_selection:
                        sns.scatterplot(
                            data=dfruns,
                            x="rate",
                            y="time_resolution",
                            style="label",
                            ax=axs["timeres"],
                            hue="label",
                            palette="tab10",
                            legend=None,
                        )
                        ylim = (0, dfruns.time_resolution.max() + 2)
                        axs["timeres"].set(
                            xlabel="Rate [Hz/cm$^2$]",
                            ylabel="Time [ns]",
                            title="Time resolution",
                            xlim=(0, None),
                            ylim=ylim,
                        )

                    if "effmax" in plot_selection:
                        sns.scatterplot(
                            data=dfruns,
                            x="rate",
                            y="effmax",
                            style="label",
                            ax=axs["effmax"],
                            hue="label",
                            palette="tab10",
                            legend=None,
                        )
                        axs["effmax"].set(
                            xlabel="Rate [Hz/cm$^2$]",
                            ylabel="Efficiency [%]",
                            title="Maximum efficiency",
                            xlim=(0, None),
                            ylim=(70, 100),
                        )

                    if "charge" in plot_selection:
                        sns.scatterplot(
                            data=dfruns,
                            x="rate",
                            y="charge_per_count",
                            style="label",
                            ax=axs["charge"],
                            hue="label",
                            palette="tab10",
                            legend=None,
                        )
                        axs["charge"].set(
                            xlabel="Rate [Hz/cm$^2$]",
                            ylabel="Charge [pC]",
                            title="Charge per count",
                            xlim=(0, None),
                            ylim=(0, 300),
                        )

                    if "abs" in plot_selection:
                        sns.barplot(
                            data=dfruns,
                            x="upstream_abs",
                            y="currents_standard_mean",
                            ax=axs["abs"],
                            hue="label",
                            palette="tab10",
                        )
                        axs["abs"].set(ylabel="Currents [uA]", title="Currents vs. ABS")
                        axs["abs"].get_legend().remove()

                    fig.subplots_adjust(wspace=0.3, hspace=0.3)
                    st.pyplot(fig)

        elif selected_plot == "📊 Distribution plots":
            fig, ax = plt.subplots()
            dfe = pd.read_sql(
                """
                select data.charge, data.label
                from (
                    SELECT e.charge,
                    'RPC' || r.rpc || ' - ' || r.gas_mixture AS label,
                    dense_rank() over (order by (r.rpc, r.gas_mixture)) as rank
                    FROM acquisitions a, runs r, events e
                    WHERE a.run_id = r.id AND e.acquisition_id = a.id
                    AND r.id = ANY(%s)
                    AND e.is_detected
                    AND a.voltage <= (r.working_point + 100)
                    AND a.voltage > (r.working_point - 100)
                ) data
                where data.rank < 5
            """,
                conn,
                params=[ids],
            )

            if dfe.empty:
                st.error("Error: no data found to plot")
                st.stop()

            sns.histplot(
                data=dfe,
                x="charge",
                log_scale=True,
                hue="label",
                palette="tab10",
                fill=False,
                stat="density",
                element="step",
                common_norm=False,
                bins=150,
                ax=ax,
            )
            sns.move_legend(ax, loc="lower left", bbox_to_anchor=(0, 1.05))
            ax.set(ylabel="Normalized counts", xlabel="Charge [pC]")
            st.pyplot(fig)

            with mpl.rc_context(
                rc={
                    "axes.facecolor": (0, 0, 0, 0),
                    "xtick.top": False,
                    "xtick.bottom": True,
                }
            ):
                g = sns.FacetGrid(
                    dfe,
                    hue="label",
                    palette="tab10",
                    row="label",
                    sharex=False,
                    aspect=5,
                    height=1.2,
                    despine=True,
                )
                g.set(xscale="log", xlim=(0.1, 600))
                g.map(
                    sns.histplot,
                    "charge",
                    element="step",
                    fill=False,
                    stat="density",
                    log_scale=True,
                    bins=np.linspace(-1, 2.4, 100),
                    linewidth=0.5,
                    clip_on=False,
                )
                g.despine(left=True, top=True, bottom=True)
                for ax in g.axes.ravel()[:-1]:
                    ax.xaxis.set_ticklabels([])
                    ax.set(yticks=[])
                    ax.tick_params(axis="x", length=0, which="both")

                def label(x, color, label):
                    ax = plt.gca()  # get current axis
                    ax.text(
                        0.85,
                        0.2,
                        label,
                        color=color,
                        fontsize=8,
                        ha="right",
                        va="center",
                        transform=ax.transAxes,
                    )

                # iterate grid to plot labels
                g.map(label, "label")
                g.set_ylabels("")
                g.set_titles("")
                g.set_axis_labels("Charge [pC]", "")
                g.fig.subplots_adjust(hspace=-0.654)

                sns.despine(left=True, top=True, bottom=False, ax=g.axes.ravel()[-1])
            st.pyplot(g.fig)

        elif selected_plot == "🌊 Raw Waveforms":
            # Get selected paths from list
            try:
                paths = []
                if table["selected_rows"] is not None:
                    paths = [
                        row["folder_path"]
                        for _, row in table["selected_rows"].iterrows()
                    ]

                if len(paths) > 1:
                    st.warning(
                        "Warning: only one run at the time can be checked. Only the first selected run will be displayed."
                    )
                path = paths[0]
                path = Path(path)

                hv_folders = list(path.glob("**/HV*"))
                if not hv_folders:
                    st.error("Error: no HV folders found in path")
                    st.stop()

                last_hv_folder = hv_folders[-1]
                config = olefin.EPDTRunAnalysis.config_from_folder(path)
                waves = {}
                for wavefile in last_hv_folder.glob("**/wave*.txt"):
                    wavenum = int(wavefile.stem.strip("wave"))
                    df = pd.read_csv(wavefile, header=None, names=["wave"])
                    data = df.wave.values.reshape((-1, config.acquisition.header))
                    waves[wavenum] = data
                sorted_waves = {key: waves[key] for key in sorted(waves.keys())}

                print(f"Num waves {waves}, ids = {waves.keys()}")
                fig, axs = plt.subplot_mosaic(
                    [[w] for w in waves.keys()], figsize=(4, 4 * len(waves)), dpi=300
                )
                for wavenum, wavedata in waves.items():
                    ax = axs[wavenum]
                    ax.plot(wavedata[:num_waveforms, :].T)
                    ax.set(title=f"Wave {wavenum}", xlabel="Sample", ylabel="ADC")

                st.pyplot(fig)
            except Exception as e:
                st.exception(e)

        else:
            st.warning("Select one or more rows to plot")

    else:
        st.info("Nothing to show")

wp_data = pd.read_sql(
    """
            SELECT r.id, r.name, r.rpc, r.gas_mixture, (r.config->'run'->'gamma'->>'source_on')::bool source_on,
            ROUND((r.config->'run'->'gamma'->'upstream_abs')::numeric, 1) upstream_abs,
            r.effmax, r.working_point, r.streamer_probability, 
            r.avalanche, r.streamer, r.cluster_size, r.time_resolution, r.time_over_threshold, 
            r.avalanche_streamer_separation, r.streamer_variability,
            r.folder_path
            FROM runs r
            WHERE r.id = ANY(%(ids)s)
        """,
    conn,
    params={"ids": ids},
)
with st.container():
    st.markdown("## Working point data")
    wp_table = AgGrid(wp_data, reload_data=True, key="wp_data")
    download_button = st.download_button(
        label="Download working point data", data=wp_data.to_csv()
    )
