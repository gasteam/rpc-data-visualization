FROM python:3.12
EXPOSE 8501
RUN apt-get update && apt-get install -y \
    fonts-freefont-ttf \
    && rm -rf /var/lib/apt/lists/*
WORKDIR /app
COPY requirements.txt ./requirements.txt
RUN pip3 install -r requirements.txt
COPY . .
RUN mkdir .streamlit
RUN chgrp -R 0 /app/.streamlit && \
    chmod -R g=u /app/.streamlit
USER 10001
CMD streamlit run olefin_scans.py